#!/usr/bin/env bats

setup() {
    IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"

    # required globals - stored in Pipelines repository variables
    APP_ID="${APP_ID}"
    PASSWORD="${PASSWORD}"
    TENANT_ID="${TENANT_ID}"
    RESOURCE_GROUP="${RESOURCE_GROUP}"
    NAME="${NAME}"

    # generated
    RANDOM_NUMBER=$RANDOM
    ZIP_FILE="artifact-$RANDOM_NUMBER.zip"

    # optional globals - stored in Pipelines repository variables
    SLOT="${SLOT}"

    # clean up
    echo "Clean up zip file"
    rm -f artifact-*.zip

    # create file
    echo "Create zip file"
    echo $RANDOM_NUMBER > test/code/index.html
    zip -j $ZIP_FILE test/code/*
}

teardown() {
    echo "Clean up zip file"
    rm -f artifact-*.zip
}

@test "artifact.zip file can be deployed to Azure app service" {
    run docker run \
        -e APP_ID="${APP_ID}" \
        -e PASSWORD="${PASSWORD}" \
        -e TENANT_ID="${TENANT_ID}" \
        -e RESOURCE_GROUP="${RESOURCE_GROUP}" \
        -e NAME="${NAME}" \
        -e ZIP_FILE="${ZIP_FILE}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo ${output}
    [[ "${status}" == "0" ]]

    # Verify
    run curl --silent "https://${NAME}.azurewebsites.net"

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"$RANDOM_NUMBER"* ]]
}

@test "artifact.zip file can be deployed to Azure app service slot" {
    echo "Run test"
    run docker run \
        -e APP_ID="${APP_ID}" \
        -e PASSWORD="${PASSWORD}" \
        -e TENANT_ID="${TENANT_ID}" \
        -e RESOURCE_GROUP="${RESOURCE_GROUP}" \
        -e NAME="${NAME}" \
        -e ZIP_FILE="${ZIP_FILE}" \
        -e SLOT="${SLOT}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo ${output}
    [[ "${status}" == "0" ]]

    # Verify
    run curl --silent "https://${NAME}-${SLOT}.azurewebsites.net"

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"$RANDOM_NUMBER"* ]]
}
