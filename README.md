# Bitbucket Pipelines Pipe: Azure Web Apps Deploy

### Noted: From February 26th, 2019, This pipe is deprecated and has been moved and maintained by Microsoft in the account: [https://bitbucket.org/microsoft/](https://bitbucket.org/microsoft/).

Deploys an application to [Azure Web Apps](https://azure.microsoft.com/en-gb/services/app-service/web). Azure Web Apps enables you to build and host web applications in the programming language of your choice without managing infrastructure. It offers auto-scaling and high availability.
                          
## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/azure-web-apps-deploy:0.2.1
  variables:
    APP_ID: '<string>'
    PASSWORD: '<string>'
    TENANT_ID: '<string>'
    RESOURCE_GROUP: '<string>'
    NAME: '<string>'
    ZIP_FILE: '<string>'
    # SLOT: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable              | Usage                                                       |
| ------------------------ | ----------------------------------------------------------- |
| APP_ID (*)               | The app ID, URL or name associated with the service principal required for login. |
| PASSWORD (*)             | Credentials like the service principal password, or path to certificate required for login. |
| TENANT_ID  (*)           | The AAD tenant required for login with service principal. |
| NAME (*)                 | Name of the web app you want to deploy. |
| RESOURCE_GROUP (*)       | Name of resource group. |
| ZIP_FILE (*)             | Zip file path for deployment e.g. app.zip |
| SLOT                     | Name of the slot. Defaults to the production slot if not specified. |
| DEBUG	                   | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

- Authentication is done via a service principal and you need to create one or use an existing one. See [Sign in with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/authenticate-azure-cli?view=azure-cli-latest) for more details.
- You have already created and configured the Azure web app and slot if required.

## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/azure-web-apps-deploy:0.2.1
    variables:
      APP_ID: $APP_ID
      PASSWORD: $PASSWORD
      TENANT_ID: $TENANT_ID
      RESOURCE_GROUP: $RESOURCE_GROUP
      NAME: 'my-site'
      ZIP_FILE: 'my-package.zip'
```

### Advanced example:

```yaml
script:
  - pipe: atlassian/azure-web-apps-deploy:0.2.1
    variables:
      APP_ID: $APP_ID
      PASSWORD: $PASSWORD
      TENANT_ID: $TENANT_ID
      RESOURCE_GROUP: $RESOURCE_GROUP
      NAME: 'my-site'
      ZIP_FILE: 'my-package.zip'
      SLOT: 'staging'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,azure


